# 1. Create a car dictionary with the following keys: brand, model, year of make, color (any values)
car = {
    "brand": "Honda",
    "model": "Civic",
    "year": 2022,
    "color": "black"
}

# 2. Print the following statement from the details: "I own a (color) (brand) (model) and it was made in (year_of_make)"
print(f"I own a {car['color']} {car['brand']} {car['model']} and it was made in {car['year']}")

# 3. Create a function that gets the square of a number
square_of_a_num = lambda num: num ** 2
print(f"The square of 6 is: {square_of_a_num(6)}")


print('\n')

# 4. Create a function that takes one of the following languages as its parameter:
# a. French
# b. Spanish
# c. Japanese

# Depending on which language is given, the function prints "Hello World!" in that language. Add a condition that asks the user to input a valid language if the given parameter does not match any of the above.
def hello_world_in():
    language = [ 'french', 'spanish', 'japanese' ]
    run = True

    while run:
        _input = input(f"'Hello World!' in? {language} (Enter 'q' to quit): ")

        if _input in ['q','Q']:
            run = False
            print('Program terminated')
            break

        if _input.lower() not in language:
            print(f"[Invalid input] '{_input}' not found. Try again\n")
            continue

        _input = _input.lower()
        run = False

        if _input == 'french': print('Bonjour le monde!')
        if _input == 'spanish': print('Hola Mundo!')
        if _input == 'japanese': print('「こんにちは世界」') 

hello_world_in()